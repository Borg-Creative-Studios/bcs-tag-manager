function menu(){
    document.getElementById("menu").innerHTML = '<div class="uk-inline" style="position: fixed; top: 0px; left: 0px;"><button class="uk-button uk-button-secondary" type="button" >Menu</button><div uk-dropdown="mode: click"><p><a href="./index.html"><button class="uk-button uk-button-text">Home</button></a></p><p><a href="https://getuikit.com/" target="_blank"><button class="uk-button uk-button-text">UIKit CSS</button></a></p><p><a style="margin-right: 5px;" href="https://www.linkedin.com/in/jacob-borg-94376b196/" target="_blank"><a style="margin-right: 5px;" href="https://twitter.com/borgcreative" target="_blank"> <img src="./img/twitter.svg" alt=""> </a><a href="https://www.instagram.com/borgcreativestudios/" target="_blank"> <img src="./img/instagram.svg" alt=""> </a></p></div></div>'
}

function footer(){
  document.getElementById("footer").innerHTML = '<nav class="uk-navbar-container" uk-navbar><div class="uk-navbar-center"><ul class="uk-navbar-nav"><li><a style="margin-right: 5px;" href="https://twitter.com/borgcreative" target="_blank"> <img src="./img/twitter.svg" alt=""> </a></li> <li class=""><a href="https://www.instagram.com/borgcreativestudios/" target="_blank"> <img src="./img/instagram.svg" alt=""> </a></li>  </ul></div></nav>'
}
